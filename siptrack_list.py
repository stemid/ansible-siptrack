# -*- coding: utf-8 -*-

# Copyright: (c) 2019, Stefan Midjich <swehack at gmail dot com>
# GNU General Public License v3.0+ (see COPYING or
# https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: siptrack_list

short_description: List siptrack devices by search pattern

version_added: "2.8"

description:
    - Connect to a Siptrack backend XMLRPC server and perform a search for
      a device pattern. List all results with OID and name of device. The
      OID can be used in subsequent siptrack module calls.


options:
    pattern:
        description:
            - Search pattern
        required: true
    max_results:
        description:
            - Maximum results to return
        default: 10
    search_all:
        description:
            - Search all attributes and networks, much slower, also supports
              regexp.
        default: False
    regexp:
        description:
            - Use regexp pattern, automatically selected when search_all is
              True.
        default: False

author:
    - Stefan Midjich (@stemid)
'''

EXAMPLES = '''
'''

RETURN = '''
returned:
  description: The output value that the RPC call returns
  type: string
  returned: success, changed
'''


from ansible.module_utils.basic import AnsibleModule
from siptracklib.config import SiptrackConfig
from siptracklib.connections import ConnectionManager
from siptracklib.utils import search_device


# Backwards compatibility for Python 2.
try:
    unicode()
except NameError:
    class unicode(str):
        pass


def run_module():
    """
    Ansible callback to run the module.
    """

    module_args = dict(
        pattern=dict(type='str', required=True),
        max_results=dict(type='int', default=10),
        search_all=dict(type='bool', default=False),
        regexp=dict(type='bool', default=False),
    )

    result = dict(
        changed=False,
        returned=None
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    try:
        cm = ConnectionManager(
            config=SiptrackConfig(),
            interactive=True
        )
        st = cm.connect()
    except Exception as e:
        module.fail_json(
            msg='Siptrack server connection failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    if not module.params['search_all']:
        attr_limit = ['name']
    else:
        attr_limit = []
        module.params['regexp'] = True

    try:
        devices = search_device(
            st,
            module.params['pattern'],
            attr_limit,
            module.params['regexp'],
            module.params['max_results']
        )
    except Exception as e:
        module.fail_json(
            msg='Device search failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    end_result = []
    for device in devices:
        end_result.append({
            'oid': device.oid,
            'name': device.attributes.get('name', '[UNNAMED]')
        })

    result['returned'] = end_result
    result['output'] = end_result
    result['changed'] = False

    module.exit_json(**result)


def main():
    """
    Main wrapper function for Ansible testing.
    """

    run_module()


if __name__ == '__main__':
    main()

