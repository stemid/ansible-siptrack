# Siptrack Ansible modules

Modules for working with Siptrack API from Ansible.

Open each module to see its arguments in the comments. Or look at the examples in the tests directory.

# Install

Easiest is to git clone this repo and add the path to your ansible.cfg library path. For example if you clone it to the current working directory where you run your ansible playbooks it would look like this.

    library = ./library:./ansible-siptrack

Or keep them in a ~/src directory.

    library = ~/ansible-siptrack

## Setup Siptrack

>**Important**: You must use the python3 branch of Siptrack with these modules. As of writing it is not merged into master.

Read more [in the siptrack quickstart docs](https://github.com/sii/siptrack), but essentially the modules make use of .siptrackrc in your $HOME.

So using your .siptrackrc you can define both the server to use, username, password and whether or not to retain the session or use TLS. Thus the ansible modules will never request a username or password from you.

If you're using a virtualenv make sure siptracklib is installed in the virtualenv.

### Example

How to install Siptrack python3 and ansible.

    $ git clone https://github.com/sii/siptrack -b python3
    $ cd siptrack
    $ python3 setup.py install --user
    $ cp docs/sample-siptrack-rc.conf ~/.siptrackrc
    $ python3 -m pip install --user ansible

Then you must setup your own ~/.siptrackrc file. Test it with siptrack ping command.

    $ siptrack ping
    Server said: 1570657368.319588
