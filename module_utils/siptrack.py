
def _find_category_by_name(base_node, name):
    """
    Find all device categories under a given base oid that match the given
    name.
    """

    categories = []
    for c in base_node.listChildren(include=['device category']):
        c_name = c.attributes.get('name', '[UNNAMED]')
        if c_name == name:
            categories.append(c)
    return categories


def find_devices_by_name(base_node, name):
    """
    Find all devices under a given base oid that match the given name.
    """

    devices = []
    for d in base_node.listChildren(include=['device']):
        d_name = d.attributes.get('name', '[UNNAMED]')
        if d_name == name:
            devices.append(d)
    return devices


def find_attribute_by_name(base_node, name):
    attribute = None
    valid_attributes = [
        'attribute',
        'versioned attribute',
        'encrypted attribute'
    ]

    for attr in base_node.listChildren(include=valid_attributes):
        a_name = attr.name
        if a_name == name:
            attribute = attr
            break
    return attribute


def set_attribute(base_node, **kw):
    """
    Create attribute that does not exist, or update attribute that does exist.
    """

    attr_type = kw.get('type', 'attribute')
    attr_name = kw.get('name', '[UNNAMED]')
    attr_atype = kw.get('atype')
    attr_value = kw.get('value')

    attribute = find_attribute_by_name(base_node, attr_name)

    if attribute and attribute.atype != attr_atype:
        raise ValueError(
            'Attribute exists but is of different type'
        )

    if attribute:
        attribute.value = attr_value
    else:
        attribute = base_node.add(
            attr_type,
            attr_name,
            attr_atype,
            attr_value
        )

    return attribute

