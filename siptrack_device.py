# -*- coding: utf-8 -*-

# Copyright: (c) 2019, Stefan Midjich <swehack at gmail dot com>
# GNU General Public License v3.0+ (see COPYING or
# https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: siptrack_device

short_description: Add or update device

version_added: "2.8"

description:
    - Add a new device under a specified base_oid, or if device exists update
      its attribute values.


options:
    base_oid:
        description:
            - Parent OID where device will be created. Can be a category,
              or another device to create a sub-device.
        required: true
    name:
        description:
            - Name attribute which must exist on all devices.
        required: true
    attributes:
        description:
            - List of attribute dicts that will be added to the newly created
              device. Each dict must contain the keys type, name, atype and
              value. type = [attribute, versioned attribute],
              atype = [text, bool, int].
    create_dupes:
        description:
            - By default this module will treat the name as unique and refuse
              to create another device with the same name under the same
              base_oid. This goes against Siptrack behavior which is quite
              flexible, so set create_dupes to True in order to ignore
              existing devices with same name and create dupes.
        default: False
    delete:
        description:
            - Delete device matching the given name, instead of creating it.
              Warning: This will delete all devices matching a name under the
              specified base_oid!
        default: False

author:
    - Stefan Midjich (@stemid)
'''

EXAMPLES = '''
'''

RETURN = '''
returned:
  description: The output value that the RPC call returns
  type: string
  returned: success, changed
'''


from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.siptrack import find_devices_by_name, set_attribute
from siptracklib.config import SiptrackConfig
from siptracklib.connections import ConnectionManager
from siptracklib.utils import search_device


# Backwards compatibility for Python 2.
try:
    unicode()
except NameError:
    class unicode(str):
        pass


def _create_device(base_node, module):
    """
    Create a siptrack device under the given base node, with the given
    module parameters. If device with same name exists, and create_dupes is
    False this function will update all the provided attributes on the
    existing device.
    """

    changed = False
    devices = find_devices_by_name(base_node, module.params['name'])

    if len(devices):
        device = devices[0]
    else:
        device = None

    if (not device) or (device and module.params['create_dupes']):
        try:
            device = base_node.add(
                'device'
            )
            device.add(
                'attribute',
                'name',
                'text',
                module.params['name']
            )
            changed = True
        except Exception as e:
            module.fail_json(
                msg='Device creation failed: {error}'.format(
                    error=str(e)
                ),
                **result
            )

    for attr in module.params['attributes']:
        attribute = set_attribute(
            device,
            type=attr.get('type', 'attribute'),
            name=attr.get('name'),
            atype=attr.get('atype'),
            value=attr.get('value')
        )
        if attr.get('important', False):
            set_attribute(
                attribute,
                type='attribute',
                name='important',
                atype='bool',
                value=attr.get('important')
            )

    end_result = {
        'oid': device.oid,
        'name': device.attributes.get('name', '[UNNAMED]')
    }

    return changed, end_result


def _delete_devices(base_node, module):
    """
    Delete all devices under the given base node where their name attribute
    matches the given name in module.params['name'].
    """

    changed = False
    devices_deleted = []
    for device in find_devices_by_name(base_node, module.params['name']):
        devices_deleted.append({
            'oid': device.oid,
            'name': device.attributes.get('name', '[UNNAMED]')
        })
        if not module.check_mode:
            device.delete()
            changed = True

    end_result = {
        'devices': devices_deleted
    }

    return changed, end_result


def run_module():
    """
    Ansible callback to run the module.
    """

    module_args = dict(
        base_oid=dict(type='str', required=True),
        name=dict(type='str', required=True),
        attributes=dict(type='list', default=[]),
        create_dupes=dict(type='bool', default=False),
        delete=dict(type='bool', default=False),
    )

    result = dict(
        changed=False,
        returned=None
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    try:
        cm = ConnectionManager(
            config=SiptrackConfig(),
            interactive=True
        )
        st = cm.connect()
    except Exception as e:
        module.fail_json(
            msg='Siptrack server connection failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    try:
        base_node = st.getOID(module.params['base_oid'])
    except Exception as e:
        module.fail_json(
            msg='Base OID lookup failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    if module.params['delete']:
        changed, end_result = _delete_devices(base_node, module)
    else:
        changed, end_result = _create_device(base_node, module)

    result['returned'] = end_result
    result['output'] = end_result
    result['changed'] = changed

    module.exit_json(**result)


def main():
    """
    Main wrapper function for Ansible testing.
    """

    run_module()


if __name__ == '__main__':
    main()

