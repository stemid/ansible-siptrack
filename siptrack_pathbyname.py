#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: (c) 2019, Stefan Midjich <swehack at gmail dot com>
# GNU General Public License v3.0+ (see COPYING or
# https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: siptrack_pathbyname

short_description: Lookup Siptrack path by name

version_added: "2.8"

description:
    - Provide a forward slash separated path, like the one in Siptrackweb UI
      breadcrumbs, and return the OID of its last component.


options:
    path:
        description:
            - Path in this format root/View/[devices]/Sweden/Malmö
        required: true

author:
    - Stefan Midjich (@stemid)
'''

EXAMPLES = '''
'''

RETURN = '''
returned:
  description: The output value that the RPC call returns
  type: string
  returned: success, changed
'''


from ansible.module_utils.basic import AnsibleModule
from siptracklib.config import SiptrackConfig
from siptracklib.connections import ConnectionManager


# Backwards compatibility for Python 2.
try:
    unicode()
except NameError:
    class unicode(str):
        pass


def run_module():
    module_args = dict(
        path=dict(type='str', required=True),
    )

    result = dict(
        changed=False,
        returned=None
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    try:
        cm = ConnectionManager(
            config=SiptrackConfig(),
            interactive=True
        )
        st = cm.connect()
    except Exception as e:
        module.fail_json(
            msg='Siptrack server connection failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    try:
        devices = search_device(
            st,
            module.params['pattern'],
            attr_limit,
            module.params['regexp'],
            module.params['max_results']
        )
    except Exception as e:
        module.fail_json(
            msg='Device search failed: {error}'.format(
                error=str(e)
            ),
            **result
        )

    end_result = []
    for device in devices:
        end_result.append({
            'oid': device.oid,
            'name': device.attributes.get('name', '[UNNAMED]')
        })

    result['returned'] = end_result
    result['output'] = end_result
    result['changed'] = False

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()

